﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;
/*
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;
*/
using System.Threading.Tasks;
using System.IO;



namespace HologramSpriteManager
{
    public enum AnimationType
    {
        Character,
        Animated
    }


    /*
    public class Frame
    {
        public Rectangle SourceRectangle;
        public Texture2D Sprite;
        public List<Collider> Colliders;
        public Color[] FrameColorData;
    }
    */
    public class Collider
    {
        public string group { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int width { get; set; }
        public int height { get; set; }

    }

    public class Collision
    {
        public bool bCollided = false;
        public string LocalGroup = "";
        public string ExternalGroup = "";

    }


    public class AnimatedSpriteSequences
    {
        public string Description { get; set; }
        public string sType { get; set; }
        public List<SpriteMap> SpriteMaps { get; set; }
        public List<AnimationSequence> AnimationSequences { get; set; }
        
        public AnimationType Type;

        public AnimatedSpriteSequences Clone()
        {
            AnimatedSpriteSequences ret = new AnimatedSpriteSequences();
            ret.Description =Description;
            ret.sType = sType;
            ret.SpriteMaps = SpriteMaps;
            ret.AnimationSequences = new List<AnimationSequence>();
            for (int i = 0; i < AnimationSequences.Count();i++ )
            {
                ret.AnimationSequences.Add( AnimationSequences[i].Clone());
            }
            ret.Type = Type;

            return ret;
        
        }

		public void PopulateSequence(bool fromExternal)
        {
            //load sprite maps:
            for (int i = 0; i < SpriteMaps.Count(); i++)
            {
				SpriteMaps[i].PopulateTexture(fromExternal);
            }
            //add weights
            for (int i = 0; i < AnimationSequences.Count(); i++)
            {
                AnimationSequences[i].CalcWeight();
            }

            //set enums
            switch(sType)
            {
                case "Character":
                    Type = AnimationType.Character;
                    break;
                case "Animated" :
                    Type = AnimationType.Animated;
                    break;
            }
            PopulateFrames();
        }

        string sCurrentSequence = "idle";
        public void SetAnimation(string sSequence)
        {
            sCurrentSequence = sSequence;
            for (int i = 0; i < AnimationSequences.Count(); i++)
            {
                if (AnimationSequences[i].AnimationName == sCurrentSequence)
                {
                    AnimationSequences[i].fLastAnimationStartTime = SpriteManager.GameTime;
                    break;
                }
            }
        }


        public void SetAnimation(string sSequence,bool bForceRestart)
        {
            if (!bForceRestart && sCurrentSequence == sSequence)
                return;

            SetAnimation(sSequence);
        }

        public void SetAnimationFlow(string sSequence,bool bForceRestart)
        {
            if (sCurrentSequence == sSequence)
            {
                if (!bForceRestart)
                {
                    return;
                }
                else
                {   
                    SetAnimation(sSequence);
                }
            }
            else
            {
                AnimationFrame _CurrentFrameMeta = new AnimationFrame();
                for (int i = 0; i < AnimationSequences.Count(); i++)
                {
                    if (AnimationSequences[i].AnimationName == sCurrentSequence)
                    {
                        //needs an algo to manipulate time
                        _CurrentFrameMeta = AnimationSequences[0].GetCurrentFrame();
                        //AnimationSequences[i].fLastAnimationStartTime = SpriteManager.GameTime;
                        break;
                    }
                }
                sCurrentSequence = sSequence;
                for (int i = 0; i < AnimationSequences.Count(); i++)
                {
                    if (AnimationSequences[i].AnimationName == sCurrentSequence)
                    {
                        //needs an algo to manipulate time
                        AnimationSequences[i].SetTimeToFrame(_CurrentFrameMeta);
                        //AnimationSequences[i].fLastAnimationStartTime = SpriteManager.GameTime;
                        break;
                    }
                }
            }
        }

		public async void SetAnimation(string startAnim,string endAnim,int duration)
		{
			SetAnimation (startAnim);
			await Task.Delay(duration > 0 ? duration : (int)AnimationSequences.Find(a => a.AnimationName == startAnim).AnimationTime);
			SetAnimation (endAnim);
		}

        public void PopulateFrames()
        { 
            //loop through animations
            for (int i = 0; i < AnimationSequences.Count(); i++)
            {
                AnimationSequence CurrentAnimation = AnimationSequences[i];
                //loop through frames
                for (int j = 0; j < CurrentAnimation.AnimationFrames.Count;j++ )
                {
                    AnimationFrame CurrentFrame = PopulateFrame(CurrentAnimation.GetFrameMeta(j));
                    CurrentAnimation.SetFrameData(CurrentFrame,j);
                }

            }
        }

        public AnimationFrame PopulateFrame(AnimationFrame frameMeta)
        {
            
            AnimationFrame frame = frameMeta;

            frame.SourceRectangle = SpriteMaps[frameMeta.map].GetRectangle(frameMeta.row, frameMeta.column);
            Color[] data = new Color[frame.SourceRectangle.Width * frame.SourceRectangle.Height];
            SpriteMaps[frameMeta.map].SpriteMapTexture.GetData(0, frame.SourceRectangle, data, 0, data.Length);

            frame.Sprite = new Texture2D(SpriteManager._GraphicsDeviceManager.GraphicsDevice, frame.SourceRectangle.Width, frame.SourceRectangle.Height);
            frame.Sprite.SetData(data);
            frame.FrameColorData = data;
            frame.colliders = frameMeta.colliders;
            

            return frame;
        }

        public AnimationFrame GetCurrentFrame()
        {
            //get sequence
            //zeroth one is standard (in case of spelling errors
            AnimationSequence CurrentSequence = AnimationSequences[0];
            for (int i = 0; i < AnimationSequences.Count(); i++)
            {
                if (AnimationSequences[i].AnimationName == sCurrentSequence)
                {
                    CurrentSequence = AnimationSequences[i];
                    break;
                }
            }
            return CurrentSequence.GetCurrentFrame();
        }

        /*
        /// <summary>
        /// Creates a new image from an existing image.
        /// </summary>
        /// <param name="bounds">Area to use as the new image.</param>
        /// <param name="source">Source image used for getting a part image.</param>
        /// <returns>Texture2D.</returns>
        Texture2D CreatePartImage(AnimationFrame afCurrentFrame)
        {
            //Declare variables
            Texture2D result;
            Color[]
                sourceColors,
                resultColors;


            Texture2D source = SpriteMaps[afCurrentFrame.map].SpriteMapTexture;
            Rectangle bounds = SpriteMaps[afCurrentFrame.map].GetRectangle(afCurrentFrame.row, afCurrentFrame.column);




            //Setup the result texture
            result = new Texture2D(SpriteManager._GraphicsDeviceManager.GraphicsDevice, bounds.Width, bounds.Height);
 
            //Setup the color arrays
            sourceColors = new Color[source.Height * source.Width];
            resultColors = new Color[bounds.Height * bounds.Width];
 
            //Get the source colors
            source.GetData<Color>(sourceColors);
 
            //Loop through colors on the y axis
            for (int y = bounds.Y; y < bounds.Height + bounds.Y; y++)
            {
                //Loop through colors on the x axis
                for (int x = bounds.X; x < bounds.Width + bounds.X; x++)
                {
                    //Get the current color
                    resultColors[x - bounds.X + (y - bounds.Y) * bounds.Width] =
                        sourceColors[x + y * source.Width];
                }
            }
 
            //Set the color data of the result image
            result.SetData<Color>(resultColors);
 
            //return the result
            return result;
        }*/
    }
}
