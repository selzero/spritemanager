using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace HologramSpriteManager
{
    class FontRenderer
    {
        public FontRenderer(string sPathRoot, int iNoTextures)
        {
            _texture = new Texture2D[iNoTextures];

            for (int i = 0; i < iNoTextures; i++)
            {
                var TexturePath = sPathRoot + "_" + i.ToString();
                _texture[i] = SpriteManager.ContentShell.Load<Texture2D>(TexturePath);
            }

            string fontFilePath = "Content/" + sPathRoot + ".fnt";
            _fontFile = FontLoader.Load(fontFilePath);

            _characterMap = new Dictionary<char, FontChar>();

            foreach (var fontCharacter in _fontFile.Chars)
            {
                char c = (char)fontCharacter.ID;
                _characterMap.Add(c, fontCharacter);
            }
        }
        public FontRenderer(FontFile fontFile, Texture2D[] fontTexture)
        {
            _fontFile = fontFile;
            _texture = fontTexture;
            _characterMap = new Dictionary<char, FontChar>();

            foreach (var fontCharacter in _fontFile.Chars)
            {
                char c = (char)fontCharacter.ID;
                _characterMap.Add(c, fontCharacter);
            }
        }

        private Dictionary<char, FontChar> _characterMap;
        private FontFile _fontFile;
        private Texture2D[] _texture;
		public void DrawTextWithWidth(Vector2 startPos,int width, string text, float fScale,Color color)
        {
            if (text == "" || text == null)
            {
                return;
            }

			int dx = (int)startPos.X;
			int dy = (int)startPos.Y;
			for (int i = 0; i < text.Length; i++) {
				char c = text [i];
				FontChar fc;
				if (_characterMap.TryGetValue (c, out fc)) {
					if ((dx + (int)(fc.Width * fScale)) < startPos.X + width) {
						var sourceRectangle = new Rectangle (fc.X, fc.Y, fc.Width, fc.Height);
						Rectangle position = new Rectangle ((int)(dx + fc.XOffset), (int)(dy + fc.YOffset), (int)(fc.Width * fScale), (int)(fc.Height * fScale));
						SpriteManager.spriteBatch.Draw (_texture [fc.Page], position, sourceRectangle, color);

						int iAdvance = fc.XAdvance;
						if (iAdvance < (int)(fScale * fc.Width)) {
							iAdvance = (int)(fScale * fc.Width);
						}
						dx += iAdvance;
					} else
						return;
				}
			}
        }

		public void DrawText(Vector2 startPos, string text, float fScale)
		{
			int dx = (int)startPos.X;
			int dy = (int)startPos.Y;
			for (int i = 0; i < text.Length; i++) {
				char c = text [i];
				FontChar fc;
				if (_characterMap.TryGetValue (c, out fc)) {
						var sourceRectangle = new Rectangle (fc.X, fc.Y, fc.Width, fc.Height);
						Rectangle position = new Rectangle ((int)(dx + fc.XOffset), (int)(dy + fc.YOffset), (int)(fc.Width * fScale), (int)(fc.Height * fScale));
						SpriteManager.spriteBatch.Draw (_texture [fc.Page], position, sourceRectangle, Color.White);

						int iAdvance = fc.XAdvance;
						if (iAdvance < (int)(fScale * fc.Width)) {
							iAdvance = (int)(fScale * fc.Width);
						}
						dx += iAdvance;
				}
			}
		}
    }
}