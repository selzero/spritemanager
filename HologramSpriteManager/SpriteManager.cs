﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HologramSpriteManager
{
    class SpriteManager
    {
        public static float GameTime=0;
        public static int iXOffset = 0;
        public static int iYOffset = 0;
        public static SpriteBatch spriteBatch;
        public static ContentManager ContentShell;
        public static GraphicsDeviceManager _GraphicsDeviceManager;

        public static void Initialise(SpriteBatch sbSpriteBatch, ContentManager cmContentShell, GraphicsDeviceManager gdmGraphicsDeviceManager)
        {
            GameTime = 0;
            spriteBatch =sbSpriteBatch;
            ContentShell = cmContentShell;
            _GraphicsDeviceManager = gdmGraphicsDeviceManager;
            //_world = new World(new Vector2(0, 0.25f));
        }

        public static void Update(GameTime gameTime)
        {
            GameTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds; 
        }

    }
}
