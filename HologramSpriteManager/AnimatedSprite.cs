﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;



namespace HologramSpriteManager
{

    class AnimatedSprite
    {
        //setup
        public AnimatedSpriteSequences Sequences;
        AnimationFrame CurrentFrame;

        public int Width;
        public int Height;

        public Vector2 Position;


        public AnimatedSprite Clone()
        {
            AnimatedSprite ret = new AnimatedSprite();
            ret.Sequences = Sequences.Clone();
            return ret;
        }


        public AnimatedSprite(AnimatedSpriteSequences Meta)
        {
            Sequences = Meta;
            CurrentFrame = Sequences.GetCurrentFrame();
            
        }
        public AnimatedSprite()
        {

        }

        public void ChangeAnimation(string sAnim)
        { 
            Sequences.SetAnimation(sAnim);

        }
        public void ChangeAnimation(string sAnim,bool bForceRestart)
        {
            Sequences.SetAnimation(sAnim,bForceRestart);

        }
        public void ChangeAnimation(string sAnim,bool bForceRestart,bool bFlow)
        {
            Sequences.SetAnimationFlow(sAnim,bForceRestart);

        }

		/// <summary>
		/// Changes the animation.
		/// </summary>
		/// <param name="startAnim">Start animation.</param>
		/// <param name="endAnim">End animation.</param>
		/// <param name="duration">Duration in miliseconds.Default value is 0. If not set, it'll use the duration from json.</param>
		public void ChangeAnimation(string startAnim,string endAnim,int duration = 0)
		{
			Sequences.SetAnimation(startAnim,endAnim,duration);
		}

        public SpriteEffects ActiveEffect = SpriteEffects.None;
        public void SetEffect(SpriteEffects Effect)
        {
            ActiveEffect = Effect;
        }


        public bool CheckIntersect(AnimatedSprite External)
        { 
            Rectangle MyBounds = Bounds;
            Rectangle ExternalBounds = External.Bounds;
            if (MyBounds.Intersects (ExternalBounds) || MyBounds.Contains(ExternalBounds) || ExternalBounds.Contains(MyBounds))
            {
                return true;
            }

            return false;
        }
        public bool CheckIntersect(Rectangle ExternalBounds)
        {
            Rectangle MyBounds = Bounds;
            if (MyBounds.Intersects(ExternalBounds) || MyBounds.Contains(ExternalBounds) || ExternalBounds.Contains(MyBounds))
            {
                return true;
            }

            return false;
        }

        public Collision CheckPixelCollision(Rectangle External)
        {
            Collision ret = new Collision();
            ret.bCollided = false;
            if (!CheckIntersect(External))
            {
                return ret;
            }
            try
            {
                Color[] dataA = CurrentFrame.FrameColorData;
                

                bool bCollided = IntersectPixels(Bounds, dataA, External);
                if (bCollided)
                {
                    ret.bCollided = true;
                    ret.LocalGroup = "PixelCompare";
                    ret.ExternalGroup = "PixelCompare";
                }
            }
            catch { Console.WriteLine("Blew Up"); }
            return ret;



        }
        public Collision CheckPixelCollision(AnimatedSprite External)
        {
            Collision ret = new Collision();
            ret.bCollided = false;

            if(!CheckIntersect(External))
            {
                return ret;
            }
            try
            {
                Color[] dataA = CurrentFrame.FrameColorData;
                Color[] dataB = External.CurrentFrame.FrameColorData;

                bool bCollided = IntersectPixels(Bounds, dataA, External.Bounds, dataB);
                if (bCollided)
                {
                    ret.bCollided = true;
                    ret.LocalGroup = "PixelCompare";
                    ret.ExternalGroup = "PixelCompare";
                }
            }
            catch { Console.WriteLine("Blew Up"); }
            return ret;
        }

        static bool IntersectPixels(Rectangle rectangleA, Color[] dataA,
                                    Rectangle rectangleB, Color[] dataB)
        {
            // Find the bounds of the rectangle intersection
            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            // Check every point within the intersection bounds
            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    // Get the color of both pixels at this point
                    Color colorA = dataA[(x - rectangleA.Left) +
                                         (y - rectangleA.Top) * rectangleA.Width];
                    Color colorB = dataB[(x - rectangleB.Left) +
                                         (y - rectangleB.Top) * rectangleB.Width];

                    // If both pixels are not completely transparent,
                    if (colorA.A != 0 && colorB.A != 0)
                    {
                        // then an intersection has been found
                        return true;
                    }
                }
            }

            // No intersection found
            return false;
        }

        static bool IntersectPixels(Rectangle rectangleA, Color[] dataA,
                                    Rectangle rectangleB)
        {
            // Find the bounds of the rectangle intersection
            int top = Math.Max(rectangleA.Top, rectangleB.Top);
            int bottom = Math.Min(rectangleA.Bottom, rectangleB.Bottom);
            int left = Math.Max(rectangleA.Left, rectangleB.Left);
            int right = Math.Min(rectangleA.Right, rectangleB.Right);

            Color colorB = new Color(new Vector4(1, 1, 1, 1));

            // Check every point within the intersection bounds
            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    // Get the color of both pixels at this point
                    Color colorA = dataA[(x - rectangleA.Left) +
                                         (y - rectangleA.Top) * rectangleA.Width];

                    // If both pixels are not completely transparent,
                    if (colorA.A != 0 && colorB.A != 0)
                    {
                        // then an intersection has been found
                        return true;
                    }
                }
            }

            // No intersection found
            return false;
        }










        public Collision CheckCollision(AnimatedSprite External)
        {
            Collision ret = new Collision();

            if (CurrentFrame.colliders == null || External.CurrentFrame.colliders == null)
                return ret;

            //get mid point and total length
            int iLocalMidX = Width / 2;
            int iLocalMidY = Height / 2;
            int iExternalMidX = External.Width / 2;
            int iExternalMidY = External.Height / 2;

            //loop every local collider
            for (int i = 0; i < CurrentFrame.colliders.Count; i++)
            {
                //get local collider X
                int iLocalColX = CurrentFrame.colliders[i].x;
                if (ActiveEffect == SpriteEffects.FlipHorizontally)
                {
                    int iDiffFromMid = iLocalMidX - iLocalColX;
                    int iNewDiffFromMid = iDiffFromMid + iLocalMidX;
                    iLocalColX = iNewDiffFromMid - CurrentFrame.colliders[i].width;
                }
                //get local collider Y
                int iLocalColY = CurrentFrame.colliders[i].y;
                if (ActiveEffect == SpriteEffects.FlipVertically)
                {
                    int iDiffFromMid = iLocalMidY - iLocalColY;
                    int iNewDiffFromMid = iDiffFromMid + iLocalMidY;
                    iLocalColY = iNewDiffFromMid - CurrentFrame.colliders[i].height;
                }

                Rectangle rCurrentLocal = new Rectangle((int)Position.X + iLocalColX, (int)Position.Y + iLocalColY, CurrentFrame.colliders[i].width, CurrentFrame.colliders[i].height);
                //loop every external collider
                for (int j = 0; j < External.CurrentFrame.colliders.Count; j++)
                {
                    //get external collider X
                    int iExternalColX = External.CurrentFrame.colliders[j].x;
                    if (External.ActiveEffect == SpriteEffects.FlipHorizontally)
                    {
                        int iDiffFromMid = iExternalMidX - iExternalColX;
                        int iNewDiffFromMid = iDiffFromMid + iExternalMidX;
                        iExternalColX = iNewDiffFromMid - External.CurrentFrame.colliders[j].width;
                    }
                    //get external collider Y
                    int iExternalColY = External.CurrentFrame.colliders[j].y;
                    if (External.ActiveEffect == SpriteEffects.FlipVertically)
                    {
                        int iDiffFromMid = iExternalMidY - iExternalColY;
                        int iNewDiffFromMid = iDiffFromMid + iExternalMidY;
                        iExternalColY = iNewDiffFromMid - External.CurrentFrame.colliders[j].width;
                    }

                    Rectangle rCurrentExternal = new Rectangle((int)External.Position.X + iExternalColX, (int)External.Position.Y + iExternalColY, External.CurrentFrame.colliders[j].width, External.CurrentFrame.colliders[j].height);
                    if(rCurrentLocal.Intersects(rCurrentExternal))
                    {
                        ret.bCollided = true;
                        ret.LocalGroup = CurrentFrame.colliders[i].group;
                        ret.ExternalGroup = External.CurrentFrame.colliders[j].group;
                        return ret;
                    }
                }

            }

            return ret;
        }
        public List<Collision> CheckAllCollisions(AnimatedSprite External)
        {
            List<Collision> ret = new List<Collision>();

            if (CurrentFrame.colliders == null || External.CurrentFrame.colliders == null)
                return ret;

            //loop every local collider
            for (int i = 0; i < CurrentFrame.colliders.Count; i++)
            {
                Rectangle rCurrentLocal = new Rectangle((int)Position.X + CurrentFrame.colliders[i].x, (int)Position.Y + CurrentFrame.colliders[i].y, CurrentFrame.colliders[i].width, CurrentFrame.colliders[i].height);
                //loop every external collider
                for (int j = 0; j < External.CurrentFrame.colliders.Count; j++)
                {
                    Rectangle rCurrentExternal = new Rectangle((int)External.Position.X + External.CurrentFrame.colliders[j].x, (int)External.Position.Y + External.CurrentFrame.colliders[j].y, External.CurrentFrame.colliders[j].width, External.CurrentFrame.colliders[j].height);
                    if (rCurrentLocal.Intersects(rCurrentExternal))
                    {
                        Collision Current = new Collision();
                        Current.bCollided = true;
                        Current.LocalGroup = CurrentFrame.colliders[i].group;
                        Current.ExternalGroup = External.CurrentFrame.colliders[j].group;
                        ret.Add(Current);
                    }
                }

            }

            return ret;
        }
        public void Draw()
        {
            CurrentFrame = Sequences.GetCurrentFrame();

            Width = CurrentFrame.SourceRectangle.Width;
            Height = CurrentFrame.SourceRectangle.Height;

            //Console.WriteLine(frame.SourceRectangle);
            int X = (int)Position.X;
            int Y = (int)Position.Y;
            
            Rectangle destinationRectangle = new Rectangle(X, Y, Width, Height);

            //SpriteManager.spriteBatch.Draw(CurrentFrame.SpriteMapTexture, destinationRectangle, frame.SourceRectangle, Color.White, 0f, new Vector2(), ActiveEffect, 0);
            SpriteManager.spriteBatch.Draw(CurrentFrame.Sprite, null, destinationRectangle, null, null, 0f, null, Color.White, ActiveEffect);
        }

		public void DrawWithAlpha(Color color)
		{
			CurrentFrame = Sequences.GetCurrentFrame();
			//Console.WriteLine(frame.SourceRectangle);
            int X = (int)Position.X;
            int Y = (int)Position.Y;

            Rectangle destinationRectangle = new Rectangle(X, Y, CurrentFrame.SourceRectangle.Width, CurrentFrame.SourceRectangle.Height);
            //SpriteManager.spriteBatch.Draw(CurrentFrame.SpriteMapTexture, destinationRectangle, frame.SourceRectangle, color);
            //SpriteManager.spriteBatch.Draw(CurrentFrame.Sprite, destinationRectangle, color);
            SpriteManager.spriteBatch.Draw(CurrentFrame.Sprite, null, destinationRectangle, null, null, 0f, null, Color.White, ActiveEffect);

		}

        public Rectangle Bounds
        {
            get
            {
                if (CurrentFrame != null)
                {
                    return new Rectangle((int)Position.X, (int)Position.Y, CurrentFrame.SourceRectangle.Width, CurrentFrame.SourceRectangle.Height);

                }
                else
                {
                    return new Rectangle(0, 0, 0, 0);
                
                }
            }
        }

    }
}
