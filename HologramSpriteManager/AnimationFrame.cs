﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HologramSpriteManager
{
    public class AnimationFrame
    {
        public int weight { get; set; }
        public int map { get; set; }
        public int row { get; set; }
        public int column { get; set; }

        public Rectangle SourceRectangle;
        public Texture2D Sprite;
        public Color[] FrameColorData;


        public List<Collider> colliders;

        public bool Compare(AnimationFrame frame)
        {
            bool bRet = false;
            if (weight == frame.weight && map == frame.map && row == frame.row && column == frame.column)
            {
                bRet = true;
            }
            return bRet;
        }
    }
}
