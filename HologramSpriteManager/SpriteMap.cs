﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HologramSpriteManager
{

    public class SpriteMap
    {
        public string SpriteMapName { get; set; }
        public Texture2D SpriteMapTexture { get; set; }
        public int Columns { get; set; }
        public int Rows { get; set; }
        private int width;
        private int height;

        public void PopulateTexture(bool fromExternal)
        {
            if (fromExternal)
            {
                using (var fileStream = File.Open(SpriteMapName, FileMode.Open, FileAccess.Read))
                {
                    //SpriteMapTexture = Texture2D.FromStream (SamsungKids.SamsungKids.graphics.GraphicsDevice, fileStream); 
                }
            }
            else
                SpriteMapTexture = SpriteManager.ContentShell.Load<Texture2D>(SpriteMapName);

            width = SpriteMapTexture.Width / Columns;
            height = SpriteMapTexture.Height / Rows;
        }
        public Rectangle GetRectangle(int iFrameRow, int iFrameColumn)
        {
            return new Rectangle(width * iFrameColumn, height * iFrameRow, width, height);
        }
    }
}
