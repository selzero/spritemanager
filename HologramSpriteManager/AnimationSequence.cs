﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HologramSpriteManager
{
    public class AnimationSequence
    {
        public string AnimationName { get; set; }
        public float AnimationTime { get; set; }
        public int AnimationLoop { get; set; }
        public List<AnimationFrame> AnimationFrames { get; set; }
        //derived
        public int iTotalWeight = 0;
        public float fLastAnimationStartTime;

        public AnimationSequence Clone()
        {
            AnimationSequence ret = new AnimationSequence();

            ret.AnimationName = AnimationName;
            ret.AnimationTime = AnimationTime;
            ret.AnimationLoop = AnimationLoop;
            ret.AnimationFrames = AnimationFrames;
            ret.iTotalWeight = iTotalWeight;


            return ret;
        }

        public void CalcWeight()
        {
            iTotalWeight = 0;
            for (int i = 0; i < AnimationFrames.Count(); i++)
            {
                iTotalWeight += AnimationFrames[i].weight;
            }
        }

        public void SetTimeToFrame(AnimationFrame TargetFrame)
        {
            int iCurrentWeight = 0;
            for (int i = 0; i < AnimationFrames.Count(); i++)
            {
                iCurrentWeight += AnimationFrames[i].weight;
                bool bSameFrame = AnimationFrames[i].Compare(TargetFrame);
                if (bSameFrame)
                {

                    //what percentage of total time has passed.
                    float fPositionPercent = (iCurrentWeight / iTotalWeight);
                    //how much time is this:
                    float fTimePassed = AnimationTime * fPositionPercent;
                    fLastAnimationStartTime = SpriteManager.GameTime - fTimePassed;


                    break;
                }
            }
        }

        public void SetFrameData(AnimationFrame NewFrame, int iFrame)
        {
            AnimationFrames[iFrame] = NewFrame;
        }

        public AnimationFrame GetFrameMeta(int iFrame)
        {
            return AnimationFrames[iFrame];
        }



        public AnimationFrame GetCurrentFrame()
        {

            float fPassedTime = SpriteManager.GameTime - fLastAnimationStartTime;
            if (fPassedTime > AnimationTime)
            {
                if (AnimationLoop == 0)
                {
                    fPassedTime = AnimationTime;
                }
                else
                {
                    fLastAnimationStartTime = SpriteManager.GameTime;
                }
            }

            int iFrame = 0;
            //if zero just use first frame
            if (fPassedTime > 0)
            {
                //weighting!
                //what percentage of total time has passed.
                float fPositionPercent = (fPassedTime / AnimationTime) * 100;
                //what is the value according to out weighting count
                float fTargetValue = (fPositionPercent / 100) * iTotalWeight;
                //loop through and find the frame
                int iCurrentWeight = 0;

                for (int i = 0; i < AnimationFrames.Count(); i++)
                {
                    iCurrentWeight += AnimationFrames[i].weight;
                    if (iCurrentWeight > fTargetValue)
                    {
                        iFrame = i;
                        break;
                    }
                }
            }
            return AnimationFrames[iFrame];
        }
    }

}
